/*
Using ESP8266 chip to connect to the local Network
*/

#include <ESP8266WiFi.h>
#include <aREST.h>

aREST rest = aREST();

//my Local Wifi

const char* ssid = "maamoon_AP";
const char* password = "maamoon_AP";

//Port to listen for incoming TCP Connection
#define LISTEN_PORT 80

//Create an instance of the server inside the ESP8266 to access from the browser

WiFiServer server(LISTEN_PORT);

void setup(void){
// Start Serial

Serial.begin(115200);

//Give name and ID to device
rest.set_id("1");
rest.set_name("test");

//connect to WiFi
WiFi.begin(ssid, password);
while (WiFi.status() != WL_CONNECTED) {
	delay(500);
	Serial.print(".");
}
Serial.println("");
Serial.println("WiFi Connected");

//Start the Server
server.begin();
Serial.println("Server Started");

//Print the IP Address
Serial.println(WiFi.localIP());
}

void loop() {

//Handle REST Calls
WiFiClient client = server.available();
if (!client) return;
while (!client.available()) delay(1);
rest.handle(client);
}
