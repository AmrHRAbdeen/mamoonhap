#include "Keypad.h"

 #include <LiquidCrystal.h>

 LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
#define LED_GREEN 9
#define LED_RED 13
#define RELAY 8


const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
{'1','2','3'},
{'4','5','6'},
{'7','8','9'},
{'#','0','*'},
};
byte rowPins[ROWS] = {A0, A1, A2, A3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A4, A5, 7}; //connect to the column pinouts of the keypad
 
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);
char keycount=0;
char code[4]; //Hold pressed keys

/****LOCK**********/
void Lock()
{
    lcd.setCursor(0,1);
    lcd.print("Door Locked     ");
    delay(1500);
    lcd.setCursor(0,1);
    lcd.print("                "); //Clear Password
    lcd.setCursor(0,1);
    keycount=0;
    digitalWrite(LED_GREEN,HIGH);  //Green LED Off
    digitalWrite(LED_RED,LOW);     //Red LED On
    digitalWrite(RELAY,HIGH);       //Turn off Relay (Locked)
}

/*********** setup ***********/

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Password Access:");
  lcd.setCursor(0,1); //Move coursor to second Line
 // Turn on the cursor
  lcd.cursor();
  /*
  LED_GREEN 9
#define LED_RED 13
*/
  pinMode(LED_GREEN , OUTPUT);
  pinMode(LED_RED , OUTPUT);
  pinMode(RELAY ,OUTPUT);
  digitalWrite(RELAY,HIGH);
//  pinMode(M1,OUTPUT);
//  pinMode(M2,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  char customKey = keypad.getKey();
  if(customKey && (keycount<4) && (customKey !='#') && (customKey !='*')){
  Serial.print(customKey);
  Serial.println(" myKey");
  //lcd.print(customKey);
  lcd.print('*');
  code[keycount]=customKey;
  keycount++;
  }
 if(customKey == '*')      //Cancel/Lock Key is pressed clear display and lock
  {
    Lock();    //Lock and clear display
  }
  
  if(customKey == '0')   //Check Password and Unlock
  {
    if((code[0]=='1') && (code[1]=='2') && (code[2]=='3') && (code[3]=='4'))  //Match the password
    {
    
      digitalWrite(LED_GREEN,LOW);  //Green LED Off
     digitalWrite(LED_RED,HIGH);     //Red LED On
      digitalWrite(RELAY,LOW); 
      lcd.setCursor(0,1);
      lcd.print("Door Open");
     // delay(4000);      //Keep Door open for 4 Seconds
      //Lock();
      
    }
   /* if ((code[0]!='1') && (
    digitalWrite(LED_GREEN,HIGH);  //Green LED Off
     digitalWrite(LED_RED,LOWcode[1]!='2') && (code[2]!='3') && (code[3]!='4')) */
   else {    //Red LED On
    
      lcd.setCursor(0,1);
      lcd.print("Invalid Password");  //Display Error Message
      delay(1500); //Message delay
      Lock();   
    }
  }
    
}
