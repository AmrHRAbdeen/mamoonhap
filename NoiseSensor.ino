/* Connection Pins
Arduino    Microphone
 GND          GND
 +5V          +5V
 D3           OUT (or D0) depends on Microphone
*/

int led = 6 ;// define LED Interface
int mic = 3; // define D0 Sensor Interface
int val = 0;// define numeric variables val
boolean previousState=true; 
boolean indicate = false;
void setup ()
{
  Serial.begin(9600);
  pinMode (led, OUTPUT) ;// define LED as output interface
  pinMode (mic, INPUT) ;// output interface D0 is defined sensor
}
 
void loop ()
{
  val = digitalRead(mic);// digital interface will be assigned a value of pin 3 to read val
  Serial.println(val);
  if (val == LOW && previousState == true) // When the sound detection module detects a signal, LED flashes
  {
    indicate = !indicate;
    Serial.println("ON");
    previousState =!previousState;
    
}
  else if (val == LOW )
  {
    indicate = !indicate;
  }
  digitalWrite(led, indicate);
  
}