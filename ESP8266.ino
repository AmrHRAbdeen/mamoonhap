#include <ESP8266WiFi.h>

//////////////////////
// WiFi Definitions //
//////////////////////
const char WiFiAPPSK[] = "sparkfun"; //PASSWORD OF ESP8266WiFi

/////////////////////
// Pin Definitions //
/////////////////////
/*const int LED_PIN = 5; // Thing's onboard, green LED
const int ANALOG_PIN = A0; // The only analog pin on the Thing
const int DIGITAL_PIN = 12; // Digital pin to be read*/
String HTTP_req;          // stores the HTTP request
boolean LED_status = 0;   // state of LED, off by default

#define led1  16
#define relay1 13
#define relay2  0
int x=5;

WiFiServer server(80);  ///PORT OF WIFI

void setup() 
{
  initHardware();  ///INIT ESP8266 
  setupWiFi();   //INIT WIFI
  server.begin();   //BEGIN SERVER
 

}

void loop() 
{
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r'); 
  Serial.println(req);
  client.flush();
  
  
  

  client.flush();

  // Prepare the response. Start with the common header:
  String s = "HTTP/1.1 200 OK\r\n";
  s += "Content-Type: text/html\r\n\r\n";
  s += "<!DOCTYPE HTML>\r\n<html>\r\n";

 

s +="<title> welcome </title>";
s +="<head>";
s +="<font size=1 color=red>";
s="<button padding=\"15px 25px\"fontsize= \"24px\" textalign=\" center\" cursor=\" pointer\" outline=\"none\" color= red bgcolor= \"green\">";
s+="<center>";


s +="<p>The system starts at <time>20:00</time>.</p>";
 s += "<Hr>";
s +="<body>";

s+="<body bgcolor=powderblue>";
s +="<header>";


s+="</header>";
//s+="<meta name=viewport content=width=device-width, initial-scale=1">";
s+="<style>";
s="<button padding=\"15px 25px\"fontsize= \"24px\" textalign=\" center\" cursor=\" pointer\" outline=\"none\" color= red bgcolor= \"green\">";

s+= "<meta http-equiv='refresh' content='10'/>";
//s +="<meta name=\"viewport" content=\"width=device-width, initial-scale=1\">";
s +="<font size=1 color=blue>";
s +="<h1>MAAMOON FOR ELECTRICAL ENGINEERING </h1>";
  s += "<br>"; 
s +="<br><input type=\"button\" name=\"bl\" value=\"CONTROL SYSTEM HOME AUTOMATION\">";

  s += "<br>"; 
s += "<br><input type=\"button\" name=\"bl\" value=\"system on\" onclick=\"location.href='/syston'\">";

 s += "<br>"; 
  s += "<br><input type=\"button\" name=\"bl\" value=\"Turn LED ON\" onclick=\"location.href='/ON1'\">";
   s += "<br>"; 

   s += "<br><input type=\"button\" name=\"bl\" value=\"Turn LED OFF\" onclick=\"location.href='/OFF1'\">";
    s += "<br>";
    s += "<br><input type=\"button\" name=\"bl\" value=\"Turn LAMP ON\" onclick=\"location.href='/high'\">"; 
   //s += "<br><input type=\"button \" name=\"bl\" value=\"Turn LAMP ON\" onclick=\"location.href='/high'\">";
    s += "<br>"; 
   s += "<br><input type=\"button\" name=\"bl\" value=\"Turn LAMP OFF\" onclick=\"location.href='/low'\">";
    s += "<br>"; 
  s += "<br><input type=\"button\" name=\"bl\" value=\"Turn TV ON\" onclick=\"location.href='/tvon'\">";
   s += "<br>"; 
   s += "<br><input type=\"button\" name=\"bl\" value=\"Turn TV OFF\" onclick=\"location.href='/tvoff'\">";
    s += "<br>"; 
    s += "<br><input type=\"button\" name=\"bl\" value=\"system OFF\" onclick=\"location.href='/systoff'\">";
     s += "<br>"; 
     
   

 
 
  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disonnected");
if (req.indexOf("") != -10) {  //checks if you're on the main page


 if (req.indexOf("/high") != -1) { //checks if you clicked ON
      digitalWrite(relay1, HIGH);
      
      Serial.println("You clicked ON");
     
    }
    if (req.indexOf("/low") != -1) { //checks if you clicked OFF
      digitalWrite(relay1, LOW);
      Serial.println("You clicked OFF");
      s += "LED is now EGYPT";
    }
  if (req.indexOf("/OFF1") != -1) { //checks if you clicked OFF
      digitalWrite(led1, LOW);
      Serial.println("You clicked OFF");
    }
    
    if (req.indexOf("/ON1") != -1) { //checks if you clicked ON
      digitalWrite(led1, HIGH);
      Serial.println("You clicked ON");
    }
     if (req.indexOf("/tvon") != -1) { //checks if you clicked OFF
       digitalWrite(relay2, HIGH);
      Serial.println("You clicked OFF");
    }
  if (req.indexOf("/tvoff") != -1) { //checks if you clicked OFF
    digitalWrite(relay2, LOW);
     
      Serial.println("You clicked OFF");
    }
    if (req.indexOf("/systoff") != -1) { //checks if you clicked OFF
    digitalWrite(relay2, LOW);
      digitalWrite(led1, LOW);
      digitalWrite(relay1, LOW);
      Serial.println("You clicked OFF");
    }
    if (req.indexOf("/syston") != -1) { //checks if you clicked OFF
    digitalWrite(relay2, HIGH);
      digitalWrite(led1, HIGH);
      digitalWrite(relay1, HIGH);
      Serial.println("You clicked OFF");
    }
    
  }

  else {
    Serial.println("invalid request");
   client.stop();
    return;
  }
  s +="</style>";
s +="</body>";
 s +="</font>";
s+="</center>";
 s += "</html>\n"; 
  
}

void setupWiFi()
{
  WiFi.mode(WIFI_AP);

  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                 String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  macID.toUpperCase();
  String AP_NameString = "ESP8266 Thing " + macID;

  char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);

  for (int i=0; i<AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);

  WiFi.softAP(AP_NameChar, WiFiAPPSK);
}

void initHardware()
{
  Serial.begin(115200);
 // pinMode(DIGITAL_PIN, INPUT_PULLUP);
  //pinMode(LED_PIN, OUTPUT);
  //digitalWrite(LED_PIN, LOW);
   pinMode(led1,OUTPUT);
  pinMode(relay1,OUTPUT);
  pinMode(relay2,OUTPUT);
  
 
  
  
}
