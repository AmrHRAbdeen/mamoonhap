/*
Kindly , Put Project Description! 

Accelerometer connection pins (I2C) to Arduino are shown below:

Arduino     Accelerometer ADXL345
  A5            SCL
  A4            SDA
  3.3V          CS
  3.3V          VCC
  GND           GND
*/


#include <Wire.h>
#include <ADXL345.h>


ADXL345 adxl; //variable adxl is an instance of the ADXL345 library

int x, y, z;
int roll, pitch, rawZ;
int mappedRoll, mappedPitch;
 /* 
  *  calibration
  *  pitch -410 >>> 240
  *  roll -215 >> 410
  */

void setup() {
  Serial.begin(9600);
  adxl.powerOn();
}

void loop() {
  adxl.readAccel(&x, &y, &z); //read the accelerometer values and store them in variables  x,y,z

  roll = y - 6;
  pitch = x - 7;
  rawZ = z + 10;
  
 // if (roll < -255) roll = -255; else if (roll > 255) roll = 255;
 // if (pitch < -255) pitch = -255; else if (pitch > 255) pitch = 255;

  // Serial.print("roll= "); Serial.println(roll);
  // Serial.print("pitch= "); Serial.println(pitch);
  // Serial.print("rawZ= "); Serial.println(rawZ);
   
  mappedRoll = map(roll, -215, 410, 0, 180);
  mappedPitch = map(pitch, -410, 240, 0, 180);


  // rawY >> rotation about x axix
  // rawX >> rotation about y axix
  if( (mappedPitch>= 70 && mappedPitch<=95)  && (mappedRoll>=80 && mappedRoll<=100) ){
    Serial.write('1');
    Serial.println("Stay");
  }

  else if( (mappedRoll> 120 && mappedRoll<=180) && (mappedPitch>= 83 && mappedPitch<=95)){
    Serial.write('2');
    Serial.println("Backward");
  }

  else if( (mappedRoll>= 0 && mappedRoll<50)&& (mappedPitch>= 75 && mappedPitch<=95)){
    Serial.write('3');
    Serial.println("Forward");
  }

  else if( (mappedPitch>= 0 && mappedPitch<80)&&(mappedRoll>=80 && mappedRoll<=95)){
    Serial.write('5');
    Serial.println("Right");
  }

  else if( (mappedPitch> 95 && mappedPitch<=180)&&(mappedRoll>=83 && mappedRoll<=95)){
    Serial.write('6');
    Serial.println("Left");
  }

  else
  {
    Serial.write('1');
    Serial.println("stay");
  }
  
  Serial.print(" roll = "); Serial.println(mappedRoll); // raw data with offset
  Serial.print(" pitch = "); Serial.println(mappedPitch); // raw data with offset
 delay(500);
}
