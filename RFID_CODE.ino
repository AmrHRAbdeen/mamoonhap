/**
 * library link :
 * 
 * http://www.mediafire.com/file/xsc537gepdc209o/archive.zip
 * 
 * ----------------------------------------------------------------------------
 * This is a MFRC522 library example; see https://github.com/miguelbalboa/rfid
 * for further details and other examples.
 *
 * NOTE: The library file MFRC522.h has a lot of useful info. Please read it.
 *
 * Released into the public domain.
 * ----------------------------------------------------------------------------
 * This sample shows how to read and write data blocks on a MIFARE Classic PICC
 * (= card/tag).
 *
 * BEWARE: Data will be written to the PICC, in sector #1 (blocks #4 to #7).
 *
 *
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 *
 */
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2); 
#include <SPI.h>
#include <MFRC522.h>

constexpr uint8_t RST_PIN = 5;     // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = 53;     // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;
int A,B,C;
int buzzer=9;
/**
 * Initialize.
 */
 char UID[] = "";
 char UID1[] = "E0:4E:04:1B";
char UID2[] = "45:62:4B:BE";
char UID3[] = "44:92:52:6D";
char Name1[] = "Sara";
char Name2[] = "Randa";
char Name3[] = "Heba";
void setup() {
  pinMode(9,OUTPUT);
    Serial.begin(9600); // Initialize serial communications with the PC
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  lcd.home();
  lcd.setCursor(0, 0);  // column 0 , row 0 i.e. top line of LCD
  lcd.print("Wating card...");
  delay(200);
  lcd.setCursor(0, 1);  // column , row  column 0 , row 1 i.e. bottom line of LCD
  delay(2000);
    }


/**
 * Main loop
 */
void loop() {
  if ( !  mfrc522.PICC_IsNewCardPresent())
    return;
  if ( ! mfrc522.PICC_ReadCardSerial())
    return;
  MFRC522::PICC_Type piccType =  mfrc522.PICC_GetType( mfrc522.uid.sak);
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K)
  {
    Serial.println(F("Your tag is not of type MIFARE Classic, your card/tag can't be read :("));
    return;
  }
  String StrID = "" ;
  for (byte i = 0; i < 4; i ++)
  {
    StrID +=
      ( mfrc522.uid.uidByte[i] < 0x10 ? "0" : "") +
      String( mfrc522.uid.uidByte[i], HEX) +
      (i != 3 ? ":" : "" );
  }
  StrID.toUpperCase();
      Serial.print("Your card's UID: ");
  Serial.println(StrID);
 mfrc522.PICC_HaltA ();
 mfrc522.PCD_StopCrypto1 ();
   if (StrID == UID1)
    { analogWrite(9,100); 
      lcd.setCursor(0, 0); A++; lcd.clear();delay(50);lcd.print("Sara :"); lcd.print("Att:");lcd.print(A);
     lcd.setCursor(0, 1); if (A<6){lcd.print("left");lcd.print(5-A);}else{lcd.print("Full attend");lcd.clear(); }
   delay(2000);
    analogWrite(9,0);}
      else  if (StrID == UID2)
    {analogWrite(9,100); 
      lcd.setCursor(0, 0); B++; lcd.clear();delay(50);lcd.print("Randa :"); lcd.print("Att:");lcd.print(B);
     lcd.setCursor(0, 1); if (B<6){lcd.print("left");lcd.print(5-B);}else{lcd.print("Full attend");lcd.clear();
    
    }delay(2000);analogWrite(9,0);  }

      else if (StrID == UID3)
    {
      
      analogWrite(9,100); 
      lcd.setCursor(0, 0); C++; lcd.clear();delay(50);lcd.print("Heba :"); lcd.print("Att:");lcd.print(C);
     lcd.setCursor(0, 1); if (C<6){lcd.print("left");lcd.print(5-C);}else{lcd.print("Full attend");lcd.clear();
    }delay(2000);analogWrite(9,0);}
    else if (StrID!= UID1||StrID!= UID2||StrID!= UID3){lcd.clear(); lcd.setCursor(5, 0); lcd.print("NOT AUTHORIZED");analogWrite(9,250); }
    
    
    delay(3000);analogWrite(9,0); 
    }

   