/*
Needs project Description ?!
*/

#include <IRremote.h>
#include <LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Password and security
String pass = "1234"; //Password to unlock. be sure to edit the '5' inside '[]' to password length +1.
int chances = 3; //Chances before buzzer will start to buzz continuously.
bool afterChances = true; /*Is possible to enter password after
  running out of chances. true = yes. (not recommended to set
  false, Arduino must be restarted to enter password if set
  to false.).If set true, buzzer will stop when correct password is
  entered after running out of chances.*/


//Pins
int RECV_PIN = 6; //IR Reciever pin (PWM).
int greenLed = 7; //Green LED pin.
int redLed = 8; //Red Led pin.
int buzzer = 13; //Buzzer pin.
int forward= 9;
int reverse= 10;

//remote buttons
String btn1 = "ff30cf"; //Your button 1 IR HEX code (in lower case). eg.: ff63ad
String btn2 = "ff18e7"; //Your button 2 IR HEX code (in lower case).
String btn3 = "ff7a85";
String btn4 = "ff10ef";
String btn5 = "ff38c7";
String btn6 = "ff5aa5";
String btn7 = "ff42bd";
String btn8 = "ff4ab5";
String btn9 = "ff52ed";
String btn0 = "ffb04f";
String btnReset = "ff629d"; //Your Reset button IR hex code.
String ch_plus = "ff02fd";
String ch_minus = "ff22dd";
String Stop = "ffa25d";

//Misc
bool shouldBeep = true;
bool unlockSound = true;

/*Don't edit anything below this if you don't what you are doing.
   Somethimes, it can cause high load to your arduino.
*/
bool locked = true;
String cPass = "";
int chance = 0;
String cmp = "----ENTER PASSWSORD-----\n";
IRrecv irrecv(RECV_PIN);
decode_results results;
String str2 = "os-----\n";



void setup() {
  lcd.begin(16, 2);

  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(forward, OUTPUT);
  pinMode(reverse, OUTPUT);
  irrecv.enableIRIn();
  Serial.begin(9600);
  digitalWrite(redLed, LOW);
  digitalWrite(greenLed, LOW);
  Serial.print(cmp);
   lcd.setCursor(0,0);
  lcd.print("ENTER PASSWSORD");
  lcd.setCursor(1, 1);
}



void loop() {
  
    lcd.setCursor(7, 1);

    if (irrecv.decode(&results)) {

      if (String(results.value, HEX) == btn1) {
        Serial.print("1");
        lcd.print("1");
        updatePass("1");

      }
      else if (String(results.value, HEX) == btn2) {
        Serial.print("2");
        lcd.setCursor(7, 1);
        lcd.print("2");
        updatePass("2");

      }
      else if (String(results.value, HEX) == btn3) {
        Serial.print("3");
        lcd.setCursor(7, 1);
        lcd.print("3");
        updatePass("3");

      }
      else if (String(results.value, HEX) == btn4) {
        Serial.print("4");       
        lcd.setCursor(7, 1);
        lcd.print("4");
        updatePass("4");
        lcd.setCursor(10, 1);
      }
      else if (String(results.value, HEX) == btn5) {
        Serial.print("5");
        lcd.setCursor(7, 1);
        lcd.print("5");
        updatePass("5");
      }
      else if (String(results.value, HEX) == btn6) {
        Serial.print("6");
        lcd.setCursor(7, 1);
        lcd.print("6");
        updatePass("6");
      }
      else if (String(results.value, HEX) == btn7) {
        Serial.print("7");
        lcd.setCursor(7, 1);
        lcd.print("7");
        updatePass("7");
      }
      else if (String(results.value, HEX) == btn8) {
        Serial.print("8");
        lcd.setCursor(7, 1);
        lcd.print("8");
        updatePass("8");
      }
      else if (String(results.value, HEX) == btn9) {
        Serial.print("9");
        lcd.setCursor(7, 1);
        lcd.print("9");
        updatePass("9");
      }
      else if (String(results.value, HEX) == btn0) {
        Serial.print("0");
        lcd.setCursor(7, 1);
        lcd.print("0");
        updatePass("0");
      }
      else if (String(results.value, HEX) == btnReset) {
        resetPass();
        Serial.println("\nRESET");
        lcd.clear();
        lcd.setCursor(5, 0);        
        lcd.print("RESET");
      }
      irrecv.resume(); // Receive the next value
    }
    
    if (chance > chances)
      digitalWrite(buzzer, HIGH);//buzzer



    if (locked == true) {
      digitalWrite(redLed, HIGH);
      digitalWrite(greenLed, LOW);
     }
    
     else if(locked==false&&String(results.value, HEX) == ch_plus) {
        Serial.print("Up");         
        lcd.clear();
        lcd.setCursor(7, 0);        
        lcd.print("UP");
      digitalWrite (forward,HIGH);
      digitalWrite (reverse,LOW);
      digitalWrite(redLed, LOW);
      digitalWrite(greenLed, HIGH);
      }
     
      else if(locked==false&&String(results.value, HEX) == ch_minus) {
        Serial.print("Down");
        lcd.clear();
        lcd.setCursor(6, 0);
        lcd.print("DOWN");

      digitalWrite (forward,LOW);
      digitalWrite (reverse,HIGH);
      digitalWrite(redLed, LOW);
      digitalWrite(greenLed, HIGH);
      }
      
      else if(locked==false&&String(results.value, HEX) == Stop) {
        Serial.print("Stop");
        lcd.clear();
        lcd.setCursor(6, 0);
        lcd.print("Stop");
      digitalWrite (forward,LOW);
      digitalWrite (reverse,LOW);
      digitalWrite(redLed, LOW);
      digitalWrite(greenLed, HIGH);}
    
  
}



bool updatePass(String ch) {
  if (locked == false)
    return false;
  beep();
  if (updatable() == true) {
    cPass += ch;
    if (cPass.length() < pass.length()) {

    }
    else {
      if (pass == cPass) {
        locked = false;
        digitalWrite(greenLed, HIGH);
        digitalWrite(redLed, LOW);
        chance = 0;
        Serial.println("\nUNLOCKED");
        lcd.setCursor (3, 0);
         lcd.clear();
         lcd.println("UNLOCKED");
        
        if (unlockSound == true) {
          delay(60);
          digitalWrite(buzzer, HIGH);
          delay(150);
          digitalWrite(buzzer, LOW);
          delay(100);
          digitalWrite(buzzer, HIGH);
          delay(200);
          digitalWrite(buzzer, LOW);
        }
      }
      else {
        cPass = "";
        chance += 1;
        digitalWrite(buzzer, HIGH);
        delay(1000);
        digitalWrite(buzzer, LOW);
        lcd.clear();
        lcd.setCursor(0,0);
        Serial.println("\nWRONG PASSWORD!");
        lcd.println("WRONG PASSWORD!");
      }
    }
  }
}

bool updatable() {
  if (chance <= chances)
    return true;
  return afterChances;
}

void resetPass() {
  cPass = "";
  locked = true;
}

void beep() {
  if (shouldBeep == true) {
    digitalWrite(buzzer, HIGH);
    delay(60);
    digitalWrite(buzzer, LOW);
  }
}
