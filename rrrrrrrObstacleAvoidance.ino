
//SoftwareSerial lm(0, 1); /*Rx  Tx*/

#include <NewPing.h>

#include <Servo.h>


#define trigger 10
#define echo 9
#define max_distance 400


NewPing UltraSonic(trigger, echo, max_distance);

float duration;
float distance;


Servo myServo;
/*motor pins*/
int pin1 =3;

int pin2 = 2;

int pin3 = 5;

int pin4 = 4;

char x;
//
#include <LiquidCrystal.h>
#include <dht11.h>

LiquidCrystal myLcd(11, 6, 13, A1, A2, A3);
dht11 DHT11;

#define DHT_PIN 7

float DHT_temprature;
float DHT_humidity;

//

 int buzzer = 8;
 int gas_sensor = A0;
 int sensorValue;
//
int sound_detector = A4;
int led = A5;
 

void setup() {
  //delay(2000);
  Serial.begin(9600);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  myServo.attach(12);
  pinMode(sound_detector, INPUT);
  pinMode(led, OUTPUT);

    for (int i = 2; i <= 5; i++)

  {
    pinMode(i, OUTPUT);

  }

  myLcd.begin(16,2);

  pinMode(buzzer, OUTPUT);
  
  
}

void loop() {
   digitalRead(sound_detector);
  if(digitalRead(sound_detector) == 1)
  {
    digitalWrite(led, HIGH);
    
  }

  else{ digitalWrite(led, LOW);}
  //
   DHT11.read(  DHT_PIN );

myLcd.setCursor(0,0);
myLcd.print("Temperature:  ");
myLcd.print(DHT11.temperature, 7);

myLcd.setCursor(0,1);
myLcd.print("RIF:  ");
myLcd.print(DHT11.humidity, 7);
myLcd.print("%");
//
sensorValue = analogRead(gas_sensor);

 if (sensorValue > 120) {
    digitalWrite(buzzer, HIGH);
  }
   else {
  
    digitalWrite(buzzer, LOW);
  }
  
 
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);

  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);

  digitalWrite(trigger, LOW);
 
  duration = pulseIn(echo, HIGH);
  distance = (duration / 2) * 0.0343;

 /* Serial.println(distance);

  if(distance < 10)
  {
    Stop();
    delay(1000);
    myServo.write(90);
  }
    if(distance > 10)
    {
      forward();
      }
    
   

    else{backward();}
  */
    //
     if (Serial.available() > 0)
  { x = Serial.read();


    if (x == 'F')
    {
      forward();
    }
    else if (x == 'B')
    {
      backward();
    }
    else if (x == 'L')
    {
      left();
    }
    else if (x == 'R')
    {
      right();
    }
    else if (x == 'G')
    {
      forward();
      left();
    }
    else if (x == 'I')
    {
      forward();
      right();
    }
    else if (x == 'H')
    {
      backward();
      left();
    }
    else if (x == 'J')
    {
      backward();
      right();
    }
    else if (x == 'S')
    {
      Stop();
    }
  }
  
}



void forward()
{
  digitalWrite(pin1, HIGH);

  digitalWrite(pin2, LOW);

  digitalWrite(pin3, LOW);

  digitalWrite(pin4, HIGH);





}
void backward()
{
  digitalWrite(pin1, LOW);

  digitalWrite(pin2, HIGH);

  digitalWrite(pin3, HIGH);

  digitalWrite(pin4, LOW);
}
void right()
{
  digitalWrite(pin1, LOW);

  digitalWrite(pin2, LOW);

  digitalWrite(pin3, LOW);

  digitalWrite(pin4, HIGH);
}
void left()
{
  digitalWrite(pin1, HIGH);

  digitalWrite(pin2, LOW);

  digitalWrite(pin3, LOW);

  digitalWrite(pin4, LOW);
}
void Stop()
{
  digitalWrite(pin1, LOW);

  digitalWrite(pin2, LOW);

  digitalWrite(pin3, LOW);

  digitalWrite(pin4, LOW);
}