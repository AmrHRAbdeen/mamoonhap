#define SW 2
#define Period 3
unsigned char cmd_buf[10];
unsigned char i,t;
byte FN,MsgGroup=0;
long randNumber=0;

void ArduinoMP3Shield_SendCMD(unsigned char *cmd_buf, unsigned len)
{
    unsigned i;
    for(i=0; i<len; i++){
        Serial.write(cmd_buf[i]);
    }
}

void PlayThis(byte FileNu)
{
    cmd_buf[0] = 0x7E;          // START
    cmd_buf[1] = 0x04;          // Length
    cmd_buf[2] = 0xA0;          // Command
    cmd_buf[3] = 0x00;          // file number high byte
    cmd_buf[4] = FileNu;        // file number low byte
    cmd_buf[5] = 0x7E;          // END
    ArduinoMP3Shield_SendCMD(cmd_buf, 6);
}

void setup(void)
{
    /** wait until arduino mp3 shield get ready */
    delay(1000);
    pinMode(SW, INPUT_PULLUP);           // set pin to input
    pinMode(Period, INPUT_PULLUP);           // set pin to input
    randomSeed(analogRead(0));
    MsgGroup = 1 ;
    Serial.begin(9600);    
    /** set volume */
    cmd_buf[0] = 0x7E;          // START
    cmd_buf[1] = 0x02;          // Length
    cmd_buf[2] = 0xA7;          // Command
    cmd_buf[3] = 0x1F;          // new volume
    cmd_buf[4] = 0x7E;          // END
    ArduinoMP3Shield_SendCMD(cmd_buf, 5);
    
    /** set play mode single */
    cmd_buf[0] = 0x7E;          // START
    cmd_buf[1] = 0x02;          // Length
    cmd_buf[2] = 0xA9;          // Command SET MODE
    cmd_buf[3] = 0x01;          // set mode
    cmd_buf[4] = 0x7E;          // END
    ArduinoMP3Shield_SendCMD(cmd_buf, 5);
    
    /** select SD card first music and play */
    cmd_buf[0] = 0x7E;          // START
    cmd_buf[1] = 0x04;          // Length
    cmd_buf[2] = 0xA0;          // Command
    cmd_buf[3] = 0x00;          // file number high byte
    cmd_buf[4] = 0x01;          // file number low byte
    cmd_buf[5] = 0x7E; // END
    ArduinoMP3Shield_SendCMD(cmd_buf, 6);
}

void loop(void)
{
    do{  
      if (digitalRead(Period) == LOW)
         {
          ++MsgGroup ;
             if (MsgGroup > 5) {MsgGroup =1;}
          delay(100);
         }
             
       if (digitalRead(SW) == LOW) break;
      } while (true);           
    
      switch (MsgGroup) {
          case 1:
            randNumber = 1 ;      // opening file
            break;
          case 2:                      
            randNumber = random(2, 8);   //Morning 8:12
            break;
          case 3:
            randNumber = random(9, 25);  // Afternoon 12:17
            break;
          case 4:
            randNumber = random(26, 40);  // evening 17:21
          default:            // if nothing else matches, do the default
            randNumber = 41;
          break;
        }
        FN= byte(randNumber);
        PlayThis(FN);
        delay(500);
}

