/*
Project Description:
Interfacing UltraSonic Sensor with Arduino to display the value of the
ultraSonic on the LCD.
*/
#include <NewPing.h>
#include <LiquidCrystal.h>

#define TRIGGER_PIN  7 
#define ECHO_PIN     9  
#define MAX_DISTANCE 300 
#define BUZZER 10
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); 
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  Serial.begin(9600); 
  pinMode(BUZZER,OUTPUT);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
}

void loop() {
  unsigned int uS = sonar.ping(); 
  Serial.print("Distance ==> ");
  Serial.print(uS / US_ROUNDTRIP_CM); 
  Serial.println(" cm ");
  delay(100);  
  if(uS < 100) {
    digitalWrite(BUZZER,HIGH);
  }
  if(uS>100)
  {
    digitalWrite(BUZZER,LOW);
  }
}