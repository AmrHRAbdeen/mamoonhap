// defines pins numbers
const int trigPin = 7;
const int echoPin = 6;
const int IN1=5;// left back
const int IN2=4;//LEFT forward
const int IN3=3;//right back
const int IN4=2;//right forward
const int line=8;
int x;

// defines variables
long duration;
int distance;

void setup() {
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input
Serial.begin(9600); // Starts the serial communication
pinMode(IN1,OUTPUT);
pinMode(IN2,OUTPUT);
pinMode(IN3,OUTPUT);
pinMode(IN4,OUTPUT);
pinMode(line,INPUT);

}


void loop() {
  x= digitalRead(line);
 ultrasoinic();
 if((distance<=100) && (x== HIGH))
 {
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,HIGH);
  digitalWrite(IN3,LOW);
  digitalWrite(IN4,HIGH);
 }
 else if((distance > 100) && (x== HIGH))
 {
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,HIGH);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
 }
 else if (x== LOW)
 {
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
 }
 }
 /* if(x==HIGH)
 {
  Serial.println("low");
 }
 else if(x==LOW)
 {
  Serial.println("high");
 }*/

int ultrasoinic()
{
digitalWrite(trigPin, LOW);
delayMicroseconds(2);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);
// Calculating the distance
distance= duration*0.034/2;
// Prints the distance on the Serial Monitor
Serial.print("Distance: ");
Serial.println(distance);

  
}

